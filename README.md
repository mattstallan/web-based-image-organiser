## Installation (XAMPP)

It is highly recommended that you install [XAMPP](https://www.apachefriends.org/download.html) for your apache server.

Once XAMPP is installed you will need to extract the files to **htdocs** folder (Default "C:\xampp\htdocs" for windows installation), make a new directory **imger**, this will be accessed at *http://localhost/imger/index.php*

You will need to enable **SQLite3** extension for PHP, this can be done by editing the **php.ini** file through the XAMPP apache **Config** button.

Once you have **php.ini** open, find *extension=sqlite3* and uncomment, save the **php.ini** file and [Start] the server with the XAMPP Control Panel

Open a browser and go to http://localhost/imger/index.php

### Unable to use port 80
If you required to run apache on another port (port 80 is used by another service perhaps), you will need to change the *Listen* port in the **httpd.conf** file through the XAMPP apache **Config** button. Once you have edited the file start the server and navigate http://localhost:[PORT]/imger/index.php where [PORT] is the new port that you have designated.

Once working you can enable apache to run as a service (Run as Adminstrator to install as service).