<?php
include(__DIR__ . "/php/dbconn.php");
?>
<!DOCTYPE html>
<html>
<head>
	<link rel= "icon" href="logo/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" title="Site Style" href="css/style.css" type="text/css" />
	<link href="./css/all.css" rel="stylesheet" type="text/css">
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/imgorg.js"></script>
</head>
<body>

	<div id="overlay"></div>

	<div id="modal2">
		<div><i class="fas fa-sync fa-spin"></i></div>
		<div>
		Scanning Directory For images
		</div>
	</div>
	<div id="container">
	<h1><a class="logo"><img src="logo/header-sm.png" /></a></h1>
		<button class="fl_right button" id="scan-dir" type="" name="scan-dir">Scan Directory</button>
		<button class="button" id="directory" type="" name="Directory"><?php if(file_exists('php/directory.json')){
																			$data = file_get_contents('php/directory.json');
																			$data2 = json_decode($data,true);
																			$dir = $data2["directory"];
																			echo $dir;
																			}else{echo "Choose Directory";}?>
		</button>
	<div class="tabs">
		<!-- Standard Search section -->
		<input type="radio" name="tabs" id="tabone" checked="checked">
		<label for="tabone">Search</label>
		<div class="tab">
		<nav class="form-style-1">
			<input id="search" type="text" name="search" placeholder="Search">
			<button class="button" id="submit-search" type="" name="submit-search">Search <i class="fas fa-search"></i></button><br/>
		</div>	
		<!-- Advanced search section -->
		<input type="radio" name="tabs" id="tabtwo">
		<label for="tabtwo">Advanced Search</label>
		<div class="tab form-style-1">
			<input id="pathName" type="text" name="search" placeholder=" Full Path Name"/>
			<input id="fileName" type="text" name="search" placeholder="File Name">
			<input id="tags" type="text" name="search" placeholder="Tags">
			<input id="comments" type="text" name="search" placeholder="Comments">
			<input id="copyright" type="text" name="search" placeholder="Copyright">
			<input id="location" type="text" name="search" placeholder="Location">
			<input id="rating" type="text" name="search" placeholder="Rating">
			<input id="title" type="text" name="search" placeholder="Title">
			<button class="button" id="adv-search" type="" name="adv-search">Adv Search <i class="fas fa-search-plus"></i></button>
		</div>
		</div>	
		<div class="search_categories">
			<div class="select">
			<select name="search_categories" id="mySort">
				<option value="pathName">Sort By</option>
				<option value="fileName">File Name</option>
				<option value="rating">Rating</option>
				<option value="location">Location</option>
				<option value="CreationDate">Creation Date</option>
				<option value="title">Title</option>
				</select>
			</div>
		</div>
		<div class="radio-select">
			<label>
				<input type="radio" name="order" id="orderAsc" value="Asc" class="form-radio" checked>Ascending
			</label>
			<label>
				<input type="radio" name="order" id="orderDesc" value="Desc" class="form-radio">Descending
			</label>
		</div>
		<br/>
		<br/>
			
		</nav>
		<!-- Edit image section -->
		<div id="result"></div>
		<div id="editImage" class="modal">
			<div class="modal-content">
				<p class="close"><i class="fas fa-times-circle"></i></p>
					<span id="pathNameEdit" class="header"></span>
					<img id='click' class="img" />
					<div class="modal-input form-style-1">
					<p> <input id="tagsEdit" type="text" name="search" placeholder="Tags"> </p>
					<p> <input id="commentsEdit" type="text" name="search" placeholder="Comments"> </p>
					<p> <input id="copyrightEdit" type="text" name="search" placeholder="Copyright"> </p>
					<p> <input id="locationEdit" type="text" name="search" placeholder="Location"> </p>
					<p> 
						<span class="starRating">
						<input id="rating5" type="radio" name="rating" value="5">
						<label for="rating5">5</label>
						<input id="rating4" type="radio" name="rating" value="4">
						<label for="rating4">4</label>
						<input id="rating3" type="radio" name="rating" value="3">
						<label for="rating3">3</label>
						<input id="rating2" type="radio" name="rating" value="2">
						<label for="rating2">2</label>
						<input id="rating1" type="radio" name="rating" value="1">
						<label for="rating1">1</label>
						</span>
					</p>
					<p> <input id="titleEdit" type="text" name="search" placeholder="Title"> </p>
						<button class="button" id="save" ><i class="fas fa-save"></i> Save</button><br/><br/>
						<button class="button btn-nav" id="prev"><i class="fas fa-arrow-circle-left"></i> Prev</button>
						<button class="button btn-nav" id="next">Next <i class="fas fa-arrow-circle-right"></i></button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>