$(document).ready(function(){
	var searchString = '';
	var advSearch = '';
	var scanDir = false;
	var pathName = '';
	var fileName = '';
	var tags = '';
	var comments = '';
	var copyright = '';
	var rating, location , title = '';
	var tagsEdit, commentsEdit, ratingEdit, locationEdit, titleEdit = '';
	var imgsrc = '';
	var modal = document.getElementById('editImage');
	var degrees = 0;
	var id = 0;
	var max = 0;
	var sortBy;
	var orderBy;
	var directory;
	
	
	/**
	 * handles simple search functionality
	 */
	$("#submit-search").click(function(){	
		
		searchString = $("#search").val();
		sortBy = $("#mySort").val();
		orderBy = $('input[name=order]:checked').val();
		//$("#result").html("");
		$("#result").load("php/search.php", {
			newSearchString: searchString,
			sortBy: sortBy,
			orderBy: orderBy,
			directory: directory
		});
	});

	/**
	 * handles scanning of directory
	 */
	$("#scan-dir").click(function(){
		$.get("./php/directory.json")
			.done(function() { 
			// json file exists
				scanDir = true;
				$("#overlay").css("display","block");
				$("#modal2").css("display","block");
				// call dbconn.php to scan directory
				$(this).load("php/dbconn.php", {
				scanDir: scanDir,
				directory: directory
				}, function(){
					$("#overlay").css("display","none");
					$("#modal2").css("display","none");
					$("#scan-dir").html("Rescan");
					}
				)
			})
			//json file doesnt exist call getDirectory
			.fail(getDirectory)
		
	});
	

	/**
	 * handles the advanced search functionality
	 */
	$("#adv-search").click(function(){
		sortBy = $("#mySort").val();
		orderBy = $('input[name=order]:checked').val();
		pathName = $("#pathName").val();
		fileName = $("#fileName").val();
		tags = $("#tags").val();
		comments = $("#comments").val();
		copyright = $("#copyright").val();
		rating = $("#rating").val();
		location = $("#location").val();
		title = $("#title").val();
		advSearch = 'true';
		$("#result").load("php/search.php", {
			advSearch: advSearch,
			pathName: pathName,
			fileName: fileName,
			tags: tags,
			comments: comments,
			copyright: copyright,
			rating: rating,
			location: location,
			title: title,
			sortBy: sortBy,
			orderBy: orderBy,
			directory: directory
			
		});
	});
	/**
	* handles the edit button functionality
	*/
	$("body").on('click', '#edit',function(){
		id = $(this).closest('div').attr('id');
		imgsrc = $(this).closest('div').find("#pathName").text().substring(1);
		$(".modal").css("display", "block");	
		$("#pathNameEdit").html($(this).closest('div').find("#pathName").text());
		$("#click").attr("src", imgsrc);
		$("#tagsEdit").val($(this).closest('div').find("#tags").text());
		$("#commentsEdit").val($(this).closest('div').find("#comments").text());
		$("#copyrightEdit").val($(this).closest('div').find("#copyright").text());
		$("#locationEdit").val($(this).closest('div').find("#location").text());
		$("input[name=rating]").val([$(this).closest('div').find("#rating").text()]);
		$("#titleEdit").val($(this).closest('div').find("#title").text());
		//commentsEdit = $(this).closest('tr').find("#tags").text();
		
	})
	/**
	* handles the previous button functionality
	*/
	$("#prev").click(function(){
		max = $('.bucket').length;
		//checks if image is the first result and disables button if it is
		if(id == 0){
			id = 0
		}else{
			id = parseInt(id) -1;
		}
		//gets previous images information
		imgsrc = $("#"+id).find("#pathName").text();
		$("#pathNameEdit").text(imgsrc);
		$("#click").attr("src", imgsrc.substring(1));
		$("#tagsEdit").val($("#"+id).find("#tags").text());
		$("#commentsEdit").val($("#"+id).find("#comments").text());
		$("#copyrightEdit").val($("#"+id).find("#copyright").text());
		$("#locationEdit").val($("#"+id).find("#location").text());
		$("input[name=rating]").val([$("#"+id).find("#rating").text()]);
		$("#titleEdit").val($("#"+id).find("#title").text());
	}) 
	/**
	* handles the next button functionality
	*/
	$("#next").click(function(){
		//checks if image is last result and disables next if it is
		max = $('.bucket').length;
		if(id == max-1){
			id = max-1;
		}else{
			id = parseInt(id) + 1;
		}
		//get next images information for display
		imgsrc = $("#"+id).find("#pathName").text();
		$("#pathNameEdit").html($(this).closest('div').find("#pathName").text());
		
		$("#pathNameEdit").text(imgsrc);
		$("#click").attr("src", imgsrc.substring(1));
		$("#tagsEdit").val($("#"+id).find("#tags").text());
		$("#commentsEdit").val($("#"+id).find("#comments").text());
		$("#copyrightEdit").val($("#"+id).find("#copyright").text());
		$("#locationEdit").val($("#"+id).find("#location").text());
		$("input[name=rating]").val([$("#"+id).find("#rating").text()]);
		$("#titleEdit").val($("#"+id).find("#title").text());
	})
	//handles closing of modal 
	$(".close").click(function(){
		$(".modal").css("display", "none");
	})
	// closes the modal if anywhere but the modal is clicked
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}
	
	
	/**
	 * handles the saving of meta data and preforms a new search to refresh the data
	 */
	$("#save").click(function(){
		
		orderBy = $('input[name=order]:checked').val();
		searchString = $("#search").val();
		sortBy = $("#mySort").val();
		pathName = $("#pathName").val();
		fileName = $("#fileName").val();
		tags = $("#tags").val();
		comments = $("#comments").val();
		copyright = $("#copyright").val();
		rating = $("#rating").val();
		location = $("#location").val();
		title = $("#title").val();
		advSearch = 'true';
		pathNameEdit = $("#pathNameEdit").text();
		tagsEdit = $("#tagsEdit").val();
		commentsEdit = $("#commentsEdit").val();
		copyrightEdit = $("#copyrightEdit").val();
		locationEdit = $("#locationEdit").val();
		ratingEdit = $('input[name=rating]:checked').val();
		titleEdit = $("#titleEdit").val();
		// sends metadata to imageUpdate to update database and file
		$(this).load("php/imageUpdate.php",{
			pathNameEdit: pathNameEdit,
			tagsEdit: tagsEdit,
			commentsEdit: commentsEdit,
			copyrightEdit: copyrightEdit,
			locationEdit: locationEdit,
			ratingEdit: ratingEdit,
			titleEdit: titleEdit,
			
		},function(){
			//preforms another search with the last searches information
			if($("#tabone").prop("checked")){
				$("#result").load("php/search.php", {
					newSearchString: searchString,
					sortBy: sortBy,
					orderBy: orderBy,
					directory: directory
				})
			}else{
				//preforms another advanced search with the last searches information
				$("#result").load("php/search.php", {
					advSearch: advSearch,
					pathName: pathName,
					fileName: fileName,
					tags: tags,
					comments: comments,
					copyright: copyright,
					rating: rating,
					location: location,
					title: title,
					sortBy: sortBy,
					orderBy: orderBy,
					directory: directory
				});
			}
		});		
	});	
	/**
	 * function to allow user to select directory
	 *@param
	 *@return
	 */
	function getDirectory(){
		directory = prompt("Select A Directory");
		while(directory == "" || directory == null){
			directory = prompt("Directory Cannot be empty String");
		}
		$(this).load("php/dbconn.php",{
			directory: directory
			
		},function(){
			$("#directory").html(directory);
		})
	}
	//attaches getDirectory function to click event of Choose directory button
	$("#directory").click(getDirectory);
		
	
	
});