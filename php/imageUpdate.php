<?php
include("dbconn.php");
//Read in POST variables.
$pathName = $_POST['pathNameEdit'];
$tags = $_POST['tagsEdit'];
$comments = $_POST['commentsEdit'];
$copyright = $_POST['copyrightEdit'];
$location = $_POST['locationEdit'];
$title = $_POST['titleEdit'];

if(isset($_POST['ratingEdit'])){	
	$rating = $_POST['ratingEdit'];
}else{
	$rating = 0;
}

// Set the IPTC metadata fields keys in an array
$iptcArr = array(
	"2#025" => $tags,
	"1#060" => $rating,
    "2#085" => $title,
    "2#116" => $copyright,
	"2#090" => "",
	"2#095" => "",
	"2#101" => "",
	"2#120" => $comments
);

//Read in the location data from the IPTC fields
$locationSplit = explode(' ',$location);
if(count($locationSplit) > 0 && $locationSplit[0] !== null){
	$iptcArr["2#090"] = $locationSplit[0];
}
if(count($locationSplit) > 1 && $locationSplit[1] !== null){
	$iptcArr["2#095"] = $locationSplit[1];
}
if(count($locationSplit) > 2 && $locationSplit[2] !== null){
	$iptcArr["2#101"] = $locationSplit[2];
}


// Convert the IPTC tags into binary code
$data = '';
foreach($iptcArr as $tag => $string)
{
	$rec_num = substr($tag,0,1);
    $tag = substr($tag, 2);	
    $data .= iptc_make_tag($rec_num, $tag, $string);
}

// Embed the IPTC data
$content = iptcembed($data, $pathName);

// Write the new image data out to the file.
$fp = fopen($pathName, "wb");
fwrite($fp, $content);
fclose($fp);

updateEditedMetadata($db, $pathName,$location,$rating,$title,$comments,$tags,$copyright);
echo"<i class='fas fa-save'></i> Save";

/**
 * Makes and embeds the iptc tags in a file
 * @param string $rec, the the iptc number for the tag 
 * @param object $data the tag that is being written
 * @param string $value	and the value of the tag
 * @return string $value the image data object
 */
function iptc_make_tag($rec, $data, $value){
    $length = strlen($value);
    $retval = chr(0x1C) . chr($rec) . chr($data);

    if($length < 0x8000)
    {
        $retval .= chr($length >> 8) .  chr($length & 0xFF);
    }
    else
    {
        $retval .= chr(0x80) . 
                   chr(0x04) . 
                   chr(($length >> 24) & 0xFF) . 
                   chr(($length >> 16) & 0xFF) . 
                   chr(($length >> 8) & 0xFF) . 
                   chr($length & 0xFF);
    }

    return $retval . $value;
}

/**
 * updateEditedMetadata
 * @param db database connection object
 * @param file file full path
 * @param location location taken
 * @param rating image rating
 * @param title image title
 * @param comments image comments
 * @param tags image tags
 * @param copyright image copyright
 * @return void
 */
function updateEditedMetadata($db, $file,$location,$rating,$title,$comments,$tags,$copyright ){
    $stmt = $db->prepare( "Update metadata SET location = :location, rating = :rating, title = :title,
						   comments = :comments, tags = :tags, copyright = :copyright WHERE pathName = :file");
    $stmt->bindParam(":file", $file);
    $stmt->bindValue(":location",$location);
    $stmt->bindValue(":rating",$rating);
    $stmt->bindValue(":title",$title);
    $stmt->bindValue(":comments",$comments);
    $stmt->bindValue(":tags",$tags);
    $stmt->bindValue(":copyright",$copyright);
    $stmt->execute();
}