<?php
class scanDir {
    //Declare variables
    static private $directories, $files, $ext_filter, $recursive;

    /**
     * Function to handle low level functionality of scanning
     * @param none 
     * @return array $files array of image file paths
     */
    static public function scan(){
        // Initialize defaults
        self::$recursive = false;
        self::$directories = array();
        self::$files = array();
        self::$ext_filter = false;

        // Check we have minimum parameters
        if(!$args = func_get_args()){
            die("Must provide a path string or array of path strings");
        }
        if(gettype($args[0]) != "string" && gettype($args[0]) != "array"){
            die("Must provide a path string or array of path strings");
        }

        // Check if recursive scan 
        if(isset($args[2]) && $args[2] == true){self::$recursive = true;}

        // Was a filter on file extensions included? | default action: return all file types
        if(isset($args[1])){
            if(gettype($args[1]) == "array"){
                self::$ext_filter = array_map('strtolower', $args[1]);
            }else if(gettype($args[1]) == "string"){
                self::$ext_filter[] = strtolower($args[1]);
            }
        }

        // Grab path(s)
        self::verifyPaths($args[0]);
        return self::$files;
    }

    /**
     * Verifies if a file path is valid
     * @param string $paths 
     * @return void
     */
    static private function verifyPaths($paths){
        $path_errors = array();
        if(gettype($paths) == "string"){
            $paths = array($paths);
        }
        //Checks if the path is a valid directory
        foreach($paths as $path){
            if(is_dir($path)){
                self::$directories[] = $path;
                $dirContents = self::find_contents($path);
            } else {
                $path_errors[] = $path;
            }
        }

        if($path_errors){
            echo "The following directories do not exists<br />";
            die(var_dump($path_errors));
        }
    }


    /**
     * Find files in the directory
     * @param string $dir directory to scan
     * @return array
     */
    static private function find_contents($dir){
        //Declare and initialise
        $result = array();
        $root = scandir($dir);

        //Loop through all directories scanning for images
        foreach($root as $value){
            if($value === '.' || $value === '..') {continue;}
            if(is_file($dir.DIRECTORY_SEPARATOR.$value)){
                if(!self::$ext_filter || in_array(strtolower(pathinfo($dir.DIRECTORY_SEPARATOR.$value, PATHINFO_EXTENSION)), self::$ext_filter)){
                    self::$files[] = $result[] = $dir.DIRECTORY_SEPARATOR.$value;
                }
                continue;
            }
            if(self::$recursive){
                foreach(self::find_contents($dir.DIRECTORY_SEPARATOR.$value) as $value) {
                    self::$files[] = $result[] = $value;
                }
            }
        }
        return $result;
    }
}
?>
