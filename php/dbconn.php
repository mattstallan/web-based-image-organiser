<?php
include("functions.php");
include("scandir.php");
//Set System variables.
$databaseName = __DIR__ .'/../Imgize.db';

//Get the chosen directory to scan for images.
if(isset($_POST["directory"])){
	$dir = $_POST["directory"];
	$jsonData = array('directory'=> $dir);
	$jsonData = json_encode($jsonData);
	file_put_contents("directory.json",$jsonData);
}else if(file_exists('directory.json')){
	$data = file_get_contents('directory.json');
	$data = json_decode($data,true);
	$dir = $data['directory'];
}

//check if database exists, if not, create it.
if(!file_exists($databaseName)){
	$db = new SQLite3($databaseName);
	createDatabase($db);
}else{
	$db = new SQLite3($databaseName);
}

//Check if scan directory was pressed, if so call scan.
if(isset($_POST['scanDir'])){
		scanDirectory($db, $dir);
}
?>