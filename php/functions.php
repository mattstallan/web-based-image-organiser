<?php
/**
 * Creates the table to hold metadata.
 * @param object $db, database object.
 * @return void
 */
function createDatabase($db){
	$query = "create table if not exists metadata (
		pathName varchar(256) primary key
		,fileName varchar
		,imageWidth int
		,imageHeight int
		,creationDate date
		,lastModified date	
		,camerManufacture varchar(256)
		,camerModel varchar(256)
		,location varchar
		,thumbnail blob	
		,rating int
		,title varchar(256)
		,comments varchar(256)
		,tags varchar(256)
		,copyright varchar(256)		
		)";
	$db->query($query);
}


/**
 * Scan directory for images
 * @param object $db, database object
 * @param string $dir, path of directory to scan.
 * @return void
 */
function scanDirectory($db, $dir){
	//Declare and initialise 
	$file_ext = array("jpg","jpeg");
	$files = scanDir::scan($dir,$file_ext,true);
	$files = array_unique($files);
	$counter = 0; 


	//Loop though each image reading in metadata.
	foreach($files as $file){
	//----------READING IN IPTC METADATA-----------//
		//read IPTC meta data into an array.
		$iptc_meta = getIptcDataTags($file);

		//Check if metadata exists, if so read it in, else assign default values.
		if(empty($iptc_meta)){
			$location = '';
			$rating = 0;
			$title = '';
			$tags = '';
			$comments = '';
			$copyright = '';
		}else{
			//Get location metadata and formate accordingly.
			if(array_key_exists('City',$iptc_meta) && array_key_exists('State',$iptc_meta) && array_key_exists('Country',$iptc_meta)){
				$location = $iptc_meta['City']. ' '.$iptc_meta['State'].' '.$iptc_meta['Country'];
			}else{
				$location = '';
			}
	
			//Get keyword metadata, which hold 'tag' data
			if(array_key_exists('Keywords',$iptc_meta)){
				//checks if Keywords is a array, if so iterate through getting multiple keywords.
				if(is_array($iptc_meta["Keywords"])){
					$c = count($iptc_meta["Keywords"]);
					for($i=0; $i<$c; $i++){
						$tags = $tags .' '. $iptc_meta["Keywords"][$i];
					}
				}else{
					//else get the single keyword
					$tags = $iptc_meta["Keywords"];
				}
			}else{
				$tags = '';
			}
			//Sanitize and prepare the tags data.
			$tags = (String) filter_var($tags, FILTER_SANITIZE_STRING);
			$tags = htmlspecialchars($tags);
			
			//Get rating metadata
			if(array_key_exists('Rating',$iptc_meta)){
				$rating = $iptc_meta['Rating'];
			}else{
				$rating = 0;
			}
			//Get Title metadata
			if(array_key_exists('DocumentTitle',$iptc_meta)){
				$title = $iptc_meta['DocumentTitle'];
			}else{
				$title = "";
			}
			//Get caption metadata, which holds comments data
			if(array_key_exists('Caption',$iptc_meta)){
				$comments = $iptc_meta['Caption'];
			}else{
				$comments = "";
			}
			//Get Copyright metadata
			if(array_key_exists('Copyright',$iptc_meta)){
				$copyright = $iptc_meta['Copyright'];
			}else{
				$copyright = "";
			}			
		}
		

	//---------READING IN EXIF METADATA-----------//
		//Read Exif data into array.
		$exif = exif_read_data($file);

		//Get camera manufacturer metadata
		if(array_key_exists("FileName",$exif)){
			$fileName = $exif['FileName'];
		}
		//Get camera make metadata
		if(array_key_exists("Make",$exif)){
			$make = $exif["Make"];
		}else{
			$make = "";
		}
		//Get camera Model metadata
		if(array_key_exists("Model",$exif)){
			$model = $exif["Model"];
		}else{
			$model = "";
		}

		$size = getimagesize($file,$info);
		$mywidth = $size[0];
		$myheight = $size[1];
		$dateMod = date("F d Y H:i:s.",filemtime($file));
		$createDate =  date("F d Y H:i:s.",filectime($file));

		//Get thumbnail and encode it in base64 if it exists, else create a thumbnail.
		$thumbnail = exif_thumbnail($file, $width, $height, $type);
		if( $thumbnail != false){
			$thumbnail = base64_encode($thumbnail); 
		}else{
			$thumbnail = base64_encode(createThumbnail($file,200));
		}
		
		//Query database for the current file.
		$selectQuery = "SELECT * FROM metadata WHERE pathName = '$file'";
		$result = $db->querySingle($selectQuery);									
		$results=$db->query("SELECT * FROM metadata where pathName = '$file'");		
		$row = $results->fetchArray(SQLITE3_ASSOC);
		
		//If file is not in database insert it in.
		if($result == NULL){
			$InsertImgQuery = "INSERT INTO metadata(pathName ,fileName, imageWidth ,imageHeight ,creationDate, lastModified ,camerManufacture 
			,camerModel ,location ,thumbnail ,rating ,title ,comments ,tags ,copyright )	
			VALUES('$file','$fileName','$mywidth', '$myheight','$createDate', '$dateMod', '$make', '$model', '$location', '$thumbnail', '$rating', '$title', '$comments', '$tags' , '$copyright' )";
			$db->query($InsertImgQuery);
		}//Else If file is in database and has been modified, update in database.
		else if($row['lastModified'] != $dateMod){
			$copyright = (String) filter_var($copyright, FILTER_SANITIZE_STRING);
			updateAllMetadata($db, $file ,$fileName, $mywidth, $myheight,$createDate,$dateMod,$make,$model,$location,$thumbnail,$rating,$title,$comments,$tags,$copyright );
		}
		//Remove image records in database if image deleted from disk.
		removeDeletedImg($db, $files);
	}
}


/**
 * Removes image records in database if they have been deleted from disk.
 * @param object $db database connection object
 * @param array $files List of files that were scanned on disk
 * @return void
 */
function removeDeletedImg($db, $files){
	//Remove image records that have been deleted from disk.
	$filesFormated = join("','", $files);
	$deleteQuery = "DELETE FROM metadata WHERE pathName NOT IN ('$filesFormated')";
	$deleteResults = $db->query("DELETE FROM metadata WHERE pathName NOT IN ('$filesFormated')");
}


/**
 * Create image thumbnail if it doesnt exist
 * @param object $image, image object
 * @param int $desired_width, set width for thumbnail 
 * @return object $thumbnail, image object
 */
function createThumbnail($image, $desired_width) {
	//Check if file exists and is Jpeg, if so create thumbnail.
	if(is_file($image) && mime_content_type($image) == 'image/jpeg' ){  
		//Set Thumbnail parameters.
		$source_image = @imageCreateFromJpeg($image);  
		$width = imagesx($source_image);
		$height = imagesy($source_image);
		$desired_height = floor($height * ($desired_width / $width));
		$virtual_image = imageCreateTrueColor($desired_width, $desired_height);
		imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	
		//Create an image object for the thumbnail.
		ob_start(); 
			imagejpeg($virtual_image); 
			$thumbnail = ob_get_contents(); 
		ob_end_clean();
	}else{														
		echo "failed to create thumbnail";
	}
	return $thumbnail;
}


/**
 * gets all iptc metadata fields keys and adds metadata int to an array.
 * @param string $image_path, path to image
 * @return array $iptc_array, array with all metadata
*/
function getIptcDataTags( $image_path ) { 
	$iptcHeaderArray = array
	(
		"1#060" => 'Rating',
		"2#025" => 'Keywords',
		"2#055" => 'DateCreated',
		"2#060" => 'TimeCreated',
		"2#085" => 'DocumentTitle',
		"2#090" => 'City',
		"2#095" => 'State',
		"2#101" => 'Country',
		"2#115" => 'Source',
		"2#116" => 'Copyright',
		"2#120" => 'Caption'	
	); 

	//Get metadata for each image
	$iptc_array = [];
    $size = getimagesize ( $image_path, $info);        
     if(is_array($info)){ 
		if(array_key_exists("APP13",$info)){	 
			$iptc = iptcparse($info["APP13"]);
			foreach (array_keys($iptc) as $s) {              
				$c = count($iptc[$s]);
				for ($i=0; $i <$c; $i++) 
				{
					if($c > 1){
						$iptc_array[$iptcHeaderArray[$s]][$i] = $iptc[$s][$i];
					}else{
						$iptc_array[$iptcHeaderArray[$s]] = $iptc[$s][$i];
					}
				}
			}
        }                  
	}
	return $iptc_array;
} 


/**
 * updates the database with the photo's new metadata
 * @param object $db database connection object
 * @param string $file file's full path name
 * @param string  $fileName filename
 * @param int $width 
 * @param int $height
 * @param date $createDate
 * @param date $dateMod
 * @param string $make 
 * @param string $model
 * @param string $location
 * @param string $thumbnail
 * @param int $rating
 * @param string $title
 * @param string $comments
 * @param string $tags
 * @param string $copyright	
 * @return void
 */
function updateAllMetadata($db, $file ,$fileName, $width, $height,$createDate,$dateMod,$make,$model,$location,$thumbnail,$rating,$title,$comments,$tags,$copyright ){
    $stmt = $db->prepare( "Update metadata SET fileName = :fileName, imageWidth = :width, imageHeight = :height,creationDate = :createDate, lastModified = :dateMod, camerManufacture = :make, 
							camerModel = :model, location = :location, thumbnail = :thumbnail, rating = :rating,
							title = :title, comments = :comments, tags = :tags, copyright = :copyright WHERE pathName = :file");
    $stmt->bindParam(":file", $file);
	$stmt->bindParam(":fileName", $fileName);
    $stmt->bindValue(":width",$width);
    $stmt->bindValue(":height", $height);
	$stmt->bindValue(":createDate",$createDate);
	$stmt->bindValue(":dateMod",$dateMod);
    $stmt->bindValue(":make", $make);
    $stmt->bindValue(":model",$model);
    $stmt->bindValue(":location",$location);
    $stmt->bindValue(":rating",$rating);
    $stmt->bindValue(":title",$title);
    $stmt->bindValue(":comments",$comments);
    $stmt->bindValue(":tags",$tags);
    $stmt->bindValue(":copyright",$copyright);
    $stmt->execute();
    $db->query("Update metadata SET thumbnail = '$thumbnail 'where pathName = '$file'");
}
?>