<?php 
include("dbconn.php");

//Run standard search if selected.
if(isset($_POST['newSearchString'])){
	//Get POST variables
	$str = $_POST['newSearchString'];
	$sort = $_POST['sortBy'];
	$order = $_POST['orderBy'];
	//Run query on database
	$results=$db->query("SELECT * 
						FROM metadata where pathName LIKE '%$str%' OR fileName LIKE '%$str%' OR tags LIKE '%$str%' OR comments LIKE '%$str%' OR copyright LIKE '%$str%' 
						OR rating LIKE '%$str%' OR location LIKE '%$str%' OR title 
						LIKE '%$str%'
						ORDER BY $sort $order");
} //Run advanced search if selected
else if(isset($_POST['advSearch'])){	
	//Get POST variables	
	$pathName = $_POST['pathName'];
	$fileName = $_POST['fileName'];
	$tags = $_POST['tags'];
	$comments = $_POST['comments'];
	$copyright = $_POST['copyright'];
	$rating = $_POST['rating'];
	$location = $_POST['location'];
	$title = $_POST['title'];
	$sort = $_POST['sortBy'];
	$order = $_POST['orderBy'];
	//Run query on database
	$results=$db->query("SELECT * FROM metadata where pathName LIKE '%$pathName%' AND fileName LIKE '%$fileName%' AND tags LIKE '%$tags%' AND comments LIKE '%$comments%' AND copyright LIKE '%$copyright%' 
						AND rating LIKE '%$rating%' AND location LIKE '%$location%' AND title LIKE '%$title%'
						ORDER BY $sort $order");					
}

	//Display pictures and info found in search
	$count = 0;
	while($row = $results->fetchArray(SQLITE3_ASSOC )){		
		echo "<div class='bucket'>";
		echo "<img src=data:image/jpeg;base64," .$row['thumbnail']." width='200' height ='200' />";
		if(strlen($row['fileName']) < 20){
			echo		"<p>".$row['fileName']."<p>";
		}else{
			echo "<p>".substr($row['fileName'], 0, 19).".."."<p>";
		}
		echo "<div id ='$count' class='hidden'>
				<span id='pathName'>". $row['pathName'] ."</span> 					
				<span id='imageWidth'>".$row['imageWidth']. "</span>   
				<span id='imageHeight'>".$row['imageHeight'] . "</span> 
				<span id='creationDate'>".$row['creationDate'] . " </span>
				<span id='camerManufacture'>".$row['camerManufacture']."</span>
				<span id='camerModel'>".$row['camerModel']."</span>
				<span id ='location'>".$row['location']."</span>
				<span id='rating' >".$row['rating']."</span>
				<span id='title'>".$row['title']."</span>
				<span id='comments' >".$row['comments']."</span>
				<span id='tags'>".$row['tags']."</span>
				<span id='copyright' >".$row['copyright']."</span>";
		echo"<button class=button id='edit'>Edit Metadata</button>";
		echo"</div></div>";
		$count++;
	}
?>